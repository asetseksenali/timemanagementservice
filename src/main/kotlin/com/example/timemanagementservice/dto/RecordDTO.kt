package com.example.timemanagementservice.dto

import java.time.Instant
import java.util.*

sealed interface RecordDTO : DTO {
    class Update(
        val startTime: Instant?,
        val endTime: Instant?
    ) : RecordDTO

    class Create(
        val carWasherId: UUID,
        val startTime: Instant,
        val endTime: Instant
    ) : RecordDTO
}