package com.example.timemanagementservice.model

import com.example.timemanagementservice.command.ChangeRecordTimeCommand
import com.example.timemanagementservice.command.CreateRecordCommand
import com.example.timemanagementservice.command.DeleteRecordForUserCommand
import com.example.timemanagementservice.event.RecordCreatedEvent
import com.example.timemanagementservice.event.RecordDeletedEvent
import com.example.timemanagementservice.event.RecordTimeChangedEvent
import com.example.timemanagementservice.exception.AuthorizationException
import com.example.timemanagementservice.exception.TimeException
import org.axonframework.commandhandling.CommandHandler
import org.axonframework.eventsourcing.EventSourcingHandler
import org.axonframework.modelling.command.AggregateIdentifier
import org.axonframework.modelling.command.AggregateLifecycle.apply
import org.axonframework.modelling.command.AggregateLifecycle.markDeleted
import org.axonframework.spring.stereotype.Aggregate
import java.time.Instant
import java.util.*

@Aggregate
class Record() {
    @AggregateIdentifier
    private lateinit var id: UUID
    private lateinit var userId: UUID
    private lateinit var carWasherId: UUID
    private lateinit var startTime: Instant
    private lateinit var endTime: Instant

    @CommandHandler
    constructor(command: CreateRecordCommand) : this() {
        apply(RecordCreatedEvent(command.id, command.userId, command.carWasherId, command.startTime, command.endTime))
    }

    @CommandHandler
    fun handle(command: ChangeRecordTimeCommand) {
        if (command.startTime != null && command.startTime.isAfter(command.endTime ?: this.endTime))
            throw TimeException("Start time is after end time")
        if (command.endTime != null && command.endTime.isBefore(command.startTime ?: this.startTime))
            throw TimeException("End time is before start time")
        if (command.userId != this.userId)
            throw AuthorizationException("Not authorized for this request")
        apply(RecordTimeChangedEvent(command.id, command.startTime, command.endTime))
    }

    @CommandHandler
    fun handle(command: DeleteRecordForUserCommand) {
        if (command.userId != this.userId)
            throw AuthorizationException("Not authorized for this request")
        apply(RecordDeletedEvent(command.id))
    }

    @EventSourcingHandler
    fun on(event: RecordCreatedEvent) {
        this.id = event.id
        this.userId = event.userId
        this.carWasherId = event.carWasherId
        this.startTime = event.startTime
        this.endTime = event.endTime
    }

    @EventSourcingHandler
    fun on(event: RecordDeletedEvent) {
        markDeleted()
    }

    @EventSourcingHandler
    fun on(event: RecordTimeChangedEvent) {
        event.startTime?.let { this.startTime = it }
        event.endTime?.let { this.endTime = it }
    }
}