package com.example.timemanagementservice.query

import com.fasterxml.jackson.annotation.JsonIgnore
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.Transient
import org.springframework.data.domain.Persistable
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import java.time.Instant
import java.util.*


@Table("records")
class RecordSummary(
    var id: UUID,
    @Column("user_id") var userId: UUID,
    @Column("car_washer_id") var carWasherId: UUID,
    @Column("start_time") var startTime: Instant,
    @Column("end_time") var endTime: Instant,
) : BaseSummary<UUID>(id)

abstract class BaseSummary<ID>(
    private var id: ID
) : Persistable<ID> {
    @Transient
    private var isNew = false

    @Id
    override fun getId() = id

    @JsonIgnore
    override fun isNew() = isNew

    fun markNew() {
        isNew = true
    }
}