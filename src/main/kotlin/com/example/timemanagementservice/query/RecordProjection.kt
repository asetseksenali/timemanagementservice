package com.example.timemanagementservice.query

import com.example.timemanagementservice.event.RecordCreatedEvent
import com.example.timemanagementservice.event.RecordDeletedEvent
import com.example.timemanagementservice.event.RecordTimeChangedEvent
import com.example.timemanagementservice.exception.AuthorizationException
import com.example.timemanagementservice.exception.DatabaseRecordNotFoundException
import com.example.timemanagementservice.repository.RecordSummaryRepository
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import org.axonframework.eventhandling.EventHandler
import org.axonframework.queryhandling.QueryHandler
import org.springframework.stereotype.Component
import reactor.kotlin.core.publisher.toMono
import java.util.concurrent.CompletableFuture

@Component
class RecordProjection(private val recordRepository: RecordSummaryRepository) {
    @EventHandler
    fun on(event: RecordCreatedEvent) {
        runBlocking {
            recordRepository.save(
                RecordSummary(
                    event.id,
                    event.userId,
                    event.carWasherId,
                    event.startTime,
                    event.endTime
                ).also { it.markNew() })
        }
    }

    @EventHandler
    fun on(event: RecordTimeChangedEvent) {
        runBlocking {
            val existingRecord = recordRepository.findById(event.id) ?: throw DatabaseRecordNotFoundException(event.id)
            event.startTime?.let { existingRecord.startTime = it }
            event.endTime?.let { existingRecord.endTime = it }
            recordRepository.save(existingRecord)
        }
    }

    @EventHandler
    fun on(event: RecordDeletedEvent) {
        runBlocking {
            recordRepository.deleteById(event.id)
        }
    }

    @QueryHandler
    fun handle(query: FetchAllRecordsForUserQuery): List<RecordSummary> {
        return runBlocking {
            recordRepository.findAllByUserId(query.userId).toList(mutableListOf())
        }
    }

    @QueryHandler
    fun handle(query: FetchAllRecordsForCarWasherQuery): List<RecordSummary> {
        return runBlocking {
            recordRepository.findAllByCarWasherId(query.carWasherId).toList(mutableListOf())
        }
    }

    @QueryHandler
    fun handle(query: FetchRecordByIdForUserQuery): CompletableFuture<RecordSummary> {
        return runBlocking {
            val record = recordRepository.findById(query.id) ?: throw DatabaseRecordNotFoundException(query.id)
            if (record.userId != query.userId) throw AuthorizationException("Forbidden query")
            record.toMono().toFuture()
        }
    }

    @QueryHandler
    fun handle(query: FetchRecordByIdForCarWasherQuery): CompletableFuture<RecordSummary> {
        return runBlocking {
            val record = recordRepository.findById(query.id) ?: throw DatabaseRecordNotFoundException(query.id)
            if (record.carWasherId != query.carWasherId) throw AuthorizationException("Forbidden query")
            record.toMono().toFuture()
        }
    }

    @QueryHandler
    fun handle(query: FetchAllActiveRecordsForCarWasherQuery): List<RecordSummary> {
        return runBlocking {
            recordRepository.findAllByCarWasherIdAndStartTimeAfter(query.carWasherId, query.now).toList(mutableListOf())
        }
    }

    @QueryHandler
    fun handle(query: FetchAllActiveRecordsForUserQuery): List<RecordSummary> {
        return runBlocking {
            recordRepository.findAllByUserIdAndStartTimeAfter(query.userId, query.now).toList(mutableListOf())
        }
    }
}