package com.example.timemanagementservice.query

import java.time.Instant
import java.util.*

sealed interface RecordQuery

data class FetchAllRecordsForUserQuery(val userId: UUID) : RecordQuery
data class FetchAllRecordsForCarWasherQuery(val carWasherId: UUID) : RecordQuery
data class FetchRecordByIdForUserQuery(val id: UUID, val userId: UUID) : RecordQuery
data class FetchRecordByIdForCarWasherQuery(val id: UUID, val carWasherId: UUID) : RecordQuery
data class FetchAllActiveRecordsForCarWasherQuery(val carWasherId: UUID, val now: Instant): RecordQuery
data class FetchAllActiveRecordsForUserQuery(val userId: UUID, val now: Instant): RecordQuery