package com.example.timemanagementservice.repository

import com.example.timemanagementservice.query.RecordSummary
import kotlinx.coroutines.flow.Flow
import org.springframework.data.repository.kotlin.CoroutineCrudRepository
import java.time.Instant
import java.util.*

interface RecordSummaryRepository : CoroutineCrudRepository<RecordSummary, UUID> {
    suspend fun findAllByUserId(userId: UUID): Flow<RecordSummary>
    suspend fun findAllByCarWasherIdAndStartTimeAfter(carWasherId: UUID, startTime: Instant): Flow<RecordSummary>
    suspend fun findAllByUserIdAndStartTimeAfter(userId: UUID, startTime: Instant): Flow<RecordSummary>
    suspend fun findAllByCarWasherId(carWasherId: UUID): Flow<RecordSummary>
    suspend fun save(record: RecordSummary): RecordSummary
}