package com.example.timemanagementservice.handler

import com.example.timemanagementservice.command.ChangeRecordTimeCommand
import com.example.timemanagementservice.command.CreateRecordCommand
import com.example.timemanagementservice.command.DeleteRecordForUserCommand
import com.example.timemanagementservice.dto.DTO
import com.example.timemanagementservice.dto.RecordDTO
import com.example.timemanagementservice.exception.DatabaseRecordNotFoundException
import com.example.timemanagementservice.exception.OAuth2AttributeNotFoundException
import com.example.timemanagementservice.exception.ValidationException
import com.example.timemanagementservice.handler.`interface`.IUserScheduleHandler
import com.example.timemanagementservice.query.FetchAllActiveRecordsForUserQuery
import com.example.timemanagementservice.query.FetchAllRecordsForUserQuery
import com.example.timemanagementservice.query.FetchRecordByIdForUserQuery
import com.example.timemanagementservice.query.RecordSummary
import com.example.timemanagementservice.validator.RecordValidator
import kotlinx.coroutines.reactor.awaitSingle
import kotlinx.coroutines.reactor.awaitSingleOrNull
import org.axonframework.extensions.reactor.commandhandling.gateway.ReactorCommandGateway
import org.axonframework.extensions.reactor.queryhandling.gateway.ReactorQueryGateway
import org.axonframework.messaging.responsetypes.ResponseTypes
import org.springframework.security.oauth2.core.OAuth2AuthenticatedPrincipal
import org.springframework.security.oauth2.server.resource.authentication.BearerTokenAuthentication
import org.springframework.stereotype.Component
import org.springframework.util.StringUtils
import org.springframework.validation.BeanPropertyBindingResult
import org.springframework.web.reactive.function.server.*
import java.time.Instant
import java.util.*

@Component
class UserScheduleHandler(
    private val commandGateway: ReactorCommandGateway,
    private val queryGateway: ReactorQueryGateway,
    private val recordValidator: RecordValidator
) : IUserScheduleHandler {
    private suspend fun getUserIdFromRequest(request: ServerRequest): UUID {
        val bearerTokenAuthentication = request.awaitPrincipal() as? BearerTokenAuthentication
            ?: throw ClassCastException("Cannot cast to BearerTokenAuthentication")
        val oauth2Principal = bearerTokenAuthentication.principal as? OAuth2AuthenticatedPrincipal
            ?: throw ClassCastException("Cannot cast to OAuth2AuthenticationPrincipal")
        return UUID.fromString(oauth2Principal.getAttribute("user_id"))
            ?: throw OAuth2AttributeNotFoundException("user_id")
    }

    override suspend fun all(request: ServerRequest): ServerResponse {
        val userId = this.getUserIdFromRequest(request)
        val records = queryGateway.query(
            FetchAllRecordsForUserQuery(userId),
            ResponseTypes.multipleInstancesOf(RecordSummary::class.java)
        ).awaitSingle()
        return ServerResponse
            .ok()
            .json()
            .bodyValueAndAwait(records)
    }

    override suspend fun allActive(request: ServerRequest): ServerResponse {
        val userId = this.getUserIdFromRequest(request)
        val now = Instant.now()
        val records = queryGateway.query(
            FetchAllActiveRecordsForUserQuery(userId, now),
            ResponseTypes.multipleInstancesOf(RecordSummary::class.java)
        ).awaitSingle()
        return ServerResponse
            .ok()
            .json()
            .bodyValueAndAwait(records)
    }

    override suspend fun create(request: ServerRequest): ServerResponse {
        val userId = this.getUserIdFromRequest(request)
        val recordDTO: RecordDTO.Create = request.awaitBodyOrNull() ?: return ServerResponse
            .badRequest()
            .buildAndAwait()
        validate(recordDTO)
        val uuid = UUID.randomUUID()
        commandGateway.send<Void>(
            CreateRecordCommand(
                uuid,
                userId,
                recordDTO.carWasherId,
                recordDTO.startTime,
                recordDTO.endTime
            )
        ).awaitSingleOrNull()
        val record = queryGateway.query(
            FetchRecordByIdForUserQuery(uuid, userId),
            ResponseTypes.instanceOf(RecordSummary::class.java)
        ).awaitSingleOrNull() ?: throw DatabaseRecordNotFoundException(uuid)
        return ServerResponse
            .ok()
            .json()
            .bodyValueAndAwait(record)
    }

    override suspend fun update(request: ServerRequest): ServerResponse {
        val userId = this.getUserIdFromRequest(request)
        val recordDTO: RecordDTO.Update = request.awaitBodyOrNull() ?: return ServerResponse
            .badRequest()
            .buildAndAwait()
        validate(recordDTO)
        val recordId = UUID.fromString(request.pathVariable("id"))
        commandGateway
            .send<Void>(ChangeRecordTimeCommand(recordId, userId, recordDTO.startTime, recordDTO.endTime))
            .awaitSingleOrNull()
        val record = queryGateway.query(
            FetchRecordByIdForUserQuery(recordId, userId),
            ResponseTypes.instanceOf(RecordSummary::class.java)
        ).awaitSingleOrNull() ?: throw DatabaseRecordNotFoundException(recordId)
        return ServerResponse
            .ok()
            .json()
            .bodyValueAndAwait(record)
    }

    override suspend fun delete(request: ServerRequest): ServerResponse {
        val userId = this.getUserIdFromRequest(request)
        val recordId = UUID.fromString(request.pathVariable("id"))
        val record = queryGateway.query(FetchRecordByIdForUserQuery(recordId, userId), RecordSummary::class.java)
            .awaitSingleOrNull() ?: throw DatabaseRecordNotFoundException(recordId)
        commandGateway.send<Void>(DeleteRecordForUserCommand(record.id, record.userId)).awaitSingleOrNull()
        return ServerResponse
            .ok()
            .json()
            .bodyValueAndAwait("OK")
    }

    override suspend fun find(request: ServerRequest): ServerResponse {
        val userId = this.getUserIdFromRequest(request)
        val recordId = UUID.fromString(request.pathVariable("id"))
        val record = queryGateway.query(FetchRecordByIdForUserQuery(recordId, userId), RecordSummary::class.java)
            .awaitSingleOrNull() ?: throw DatabaseRecordNotFoundException(recordId)
        return ServerResponse
            .ok()
            .json()
            .bodyValueAndAwait(record)
    }

    private fun <T : DTO> validate(target: T) {
        val validator = when (target) {
            is RecordDTO -> recordValidator
            else -> throw IllegalArgumentException()
        }
        val errors = BeanPropertyBindingResult(target, "target")
        validator.validate(target, errors)
        if (errors.hasErrors()) {
            throw ValidationException(errors.fieldErrors.map {
                "Error in field ${it.field}${if (StringUtils.hasText(it.rejectedValue.toString())) " for value ${it.rejectedValue}" else ""} with code ${it.code}"
            }.toSet())
        }
    }
}