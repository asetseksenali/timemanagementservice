package com.example.timemanagementservice.handler

import com.example.timemanagementservice.command.DeleteRecordForCarWasherCommand
import com.example.timemanagementservice.exception.DatabaseRecordNotFoundException
import com.example.timemanagementservice.exception.OAuth2AttributeNotFoundException
import com.example.timemanagementservice.handler.`interface`.ICarWasherScheduleHandler
import com.example.timemanagementservice.query.FetchAllActiveRecordsForCarWasherQuery
import com.example.timemanagementservice.query.FetchAllRecordsForCarWasherQuery
import com.example.timemanagementservice.query.FetchRecordByIdForCarWasherQuery
import com.example.timemanagementservice.query.RecordSummary
import kotlinx.coroutines.reactor.awaitSingle
import kotlinx.coroutines.reactor.awaitSingleOrNull
import org.axonframework.extensions.reactor.commandhandling.gateway.ReactorCommandGateway
import org.axonframework.extensions.reactor.queryhandling.gateway.ReactorQueryGateway
import org.axonframework.messaging.responsetypes.ResponseTypes
import org.springframework.security.oauth2.core.OAuth2AuthenticatedPrincipal
import org.springframework.security.oauth2.server.resource.authentication.BearerTokenAuthentication
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.*
import java.time.Instant
import java.util.*

@Component
class CarWasherScheduleHandler(
    private val commandGateway: ReactorCommandGateway,
    private val queryGateway: ReactorQueryGateway
) : ICarWasherScheduleHandler {
    private suspend fun getCarWasherIdFromRequest(request: ServerRequest): UUID {
        val bearerTokenAuthentication = request.awaitPrincipal() as? BearerTokenAuthentication
            ?: throw ClassCastException("Cannot cast to BearerTokenAuthentication")
        val oauth2Principal = bearerTokenAuthentication.principal as? OAuth2AuthenticatedPrincipal
            ?: throw ClassCastException("Cannot cast to OAuth2AuthenticationPrincipal")
        return UUID.fromString(oauth2Principal.getAttribute("car_washer_id")) ?: throw OAuth2AttributeNotFoundException(
            "car_washer_id"
        )
    }

    override suspend fun all(request: ServerRequest): ServerResponse {
        val carWasherId = this.getCarWasherIdFromRequest(request)
        val records = queryGateway.query(
            FetchAllRecordsForCarWasherQuery(carWasherId),
            ResponseTypes.multipleInstancesOf(RecordSummary::class.java)
        ).awaitSingle()
        return ServerResponse
            .ok()
            .json()
            .bodyValueAndAwait(records)
    }

    override suspend fun allActive(request: ServerRequest): ServerResponse {
        val carWasherId = this.getCarWasherIdFromRequest(request)
        val now = Instant.now()
        val records = queryGateway.query(
            FetchAllActiveRecordsForCarWasherQuery(
                carWasherId, now
            ),
            ResponseTypes.multipleInstancesOf(RecordSummary::class.java)
        ).awaitSingle()
        return ServerResponse
            .ok()
            .json()
            .bodyValueAndAwait(records)
    }

    override suspend fun find(request: ServerRequest): ServerResponse {
        val carWasherId = this.getCarWasherIdFromRequest(request)
        val recordId = UUID.fromString(request.pathVariable("id"))
        val record =
            queryGateway.query(FetchRecordByIdForCarWasherQuery(recordId, carWasherId), RecordSummary::class.java)
                .awaitSingleOrNull() ?: throw DatabaseRecordNotFoundException(recordId)
        return ServerResponse
            .ok()
            .json()
            .bodyValueAndAwait(record)
    }

    override suspend fun delete(request: ServerRequest): ServerResponse {
        val carWasherId = this.getCarWasherIdFromRequest(request)
        val recordId = UUID.fromString(request.pathVariable("id"))
        val record = queryGateway.query(FetchRecordByIdForCarWasherQuery(recordId, carWasherId), RecordSummary::class.java)
                .awaitSingleOrNull() ?: throw DatabaseRecordNotFoundException(recordId)
        commandGateway.send<Void>(DeleteRecordForCarWasherCommand(record.id, record.carWasherId)).awaitSingleOrNull()
        return ServerResponse
            .ok()
            .json()
            .bodyValueAndAwait("OK")
    }
}