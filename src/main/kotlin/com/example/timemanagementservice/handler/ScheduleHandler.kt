package com.example.timemanagementservice.handler

import com.example.timemanagementservice.exception.AuthorizationException
import com.example.timemanagementservice.handler.`interface`.ICarWasherScheduleHandler
import com.example.timemanagementservice.handler.`interface`.IScheduleHandler
import com.example.timemanagementservice.handler.`interface`.IUserScheduleHandler
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.oauth2.core.OAuth2AuthenticatedPrincipal
import org.springframework.security.oauth2.server.resource.authentication.BearerTokenAuthentication
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.awaitPrincipal

@Component
class ScheduleHandler constructor(
    @Qualifier("carWasherScheduleHandler") private val carWasherHandler: ICarWasherScheduleHandler,
    @Qualifier("userScheduleHandler") private val userHandler: IUserScheduleHandler
) : IScheduleHandler {
    private suspend fun getUserRole(request: ServerRequest): String {
        val bearerTokenAuthentication = request.awaitPrincipal() as? BearerTokenAuthentication
            ?: throw ClassCastException("Cannot cast to BearerTokenAuthentication")
        val oauth2Principal = bearerTokenAuthentication.principal as? OAuth2AuthenticatedPrincipal
            ?: throw ClassCastException("Cannot cast to OAuth2AuthenticationPrincipal")
        if (oauth2Principal.authorities.contains(SimpleGrantedAuthority("ROLE_USER"))) {
            return "USER"
        }
        if (oauth2Principal.authorities.contains(SimpleGrantedAuthority("ROLE_CAR_WASHER"))) {
            return "CAR_WASHER"
        }
        throw AuthorizationException("No role have been found")
    }

    override suspend fun all(request: ServerRequest): ServerResponse {
        return when (this.getUserRole(request)) {
            "USER" -> userHandler.all(request)
            "CAR_WASHER" -> carWasherHandler.all(request)
            else -> throw AuthorizationException("No role have been found")
        }
    }

    override suspend fun allActive(request: ServerRequest): ServerResponse {
        return when (this.getUserRole(request)) {
            "CAR_WASHER" -> carWasherHandler.allActive(request)
            else -> throw AuthorizationException("No role have been found")
        }
    }

    override suspend fun find(request: ServerRequest): ServerResponse {
        return when (this.getUserRole(request)) {
            "USER" -> userHandler.find(request)
            "CAR_WASHER" -> carWasherHandler.find(request)
            else -> throw AuthorizationException("No role have been found")
        }
    }

    override suspend fun create(request: ServerRequest): ServerResponse {
        return when (this.getUserRole(request)) {
            "USER" -> userHandler.create(request)
            else -> throw AuthorizationException("No role have been found")
        }
    }

    override suspend fun update(request: ServerRequest): ServerResponse {
        return when (this.getUserRole(request)) {
            "USER" -> userHandler.update(request)
            else -> throw AuthorizationException("No role have been found")
        }
    }

    override suspend fun delete(request: ServerRequest): ServerResponse {
        return when (this.getUserRole(request)) {
            "USER" -> userHandler.delete(request)
            "CAR_WASHER" -> carWasherHandler.delete(request)
            else -> throw AuthorizationException("No role have been found")
        }
    }
}