package com.example.timemanagementservice.handler.`interface`

import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse

interface ICarWasherScheduleHandler {
    suspend fun all(request: ServerRequest): ServerResponse
    suspend fun allActive(request: ServerRequest): ServerResponse
    suspend fun find(request: ServerRequest): ServerResponse
    suspend fun delete(request: ServerRequest): ServerResponse
}