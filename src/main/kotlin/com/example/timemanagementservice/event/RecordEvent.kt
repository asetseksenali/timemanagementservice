package com.example.timemanagementservice.event

import java.time.Instant
import java.util.*

sealed interface RecordEvent

data class RecordCreatedEvent(
    val id: UUID,
    val userId: UUID,
    val carWasherId: UUID,
    val startTime: Instant,
    val endTime: Instant
) : RecordEvent

data class RecordTimeChangedEvent(
    val id: UUID,
    val startTime: Instant?,
    val endTime: Instant?
) : RecordEvent

data class RecordDeletedEvent(val id: UUID) : RecordEvent