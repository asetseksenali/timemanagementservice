package com.example.timemanagementservice.config

import com.example.timemanagementservice.validator.RecordValidator
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class ValidationConfig {
    @Bean
    fun recordValidator(): RecordValidator = RecordValidator()
}