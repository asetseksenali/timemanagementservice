package com.example.timemanagementservice.config

import org.axonframework.config.EventProcessingConfigurer
import org.axonframework.eventhandling.TrackingEventProcessorConfiguration
import org.axonframework.messaging.StreamableMessageSource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Configuration

@Configuration
class AxonConfig {
    @Autowired
    fun configureInitialTrackingToken(processingConfigurer: EventProcessingConfigurer) {
        val tepConfig = TrackingEventProcessorConfiguration.forSingleThreadedProcessing()
            .andInitialTrackingToken(StreamableMessageSource<*>::createHeadToken)
        processingConfigurer.registerTrackingEventProcessorConfiguration {
            tepConfig
        }
    }
}