package com.example.timemanagementservice

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.context.config.annotation.RefreshScope
import org.springframework.web.reactive.config.EnableWebFlux

@RefreshScope
@EnableWebFlux
@SpringBootApplication
class TimeManagementServiceApplication

fun main(args: Array<String>) {
    runApplication<TimeManagementServiceApplication>(*args)
}
