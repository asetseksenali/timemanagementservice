package com.example.timemanagementservice.router

import com.example.timemanagementservice.exception.AuthorizationException
import com.example.timemanagementservice.exception.DatabaseRecordNotFoundException
import com.example.timemanagementservice.exception.OAuth2AttributeNotFoundException
import com.example.timemanagementservice.exception.TimeException
import com.example.timemanagementservice.handler.`interface`.IScheduleHandler
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpStatus
import org.springframework.web.reactive.function.server.bodyValueAndAwait
import org.springframework.web.reactive.function.server.buildAndAwait
import org.springframework.web.reactive.function.server.coRouter
import org.springframework.web.reactive.function.server.json

@Configuration
class RouterConfiguration {
    @Bean
    fun scheduleRouter(scheduleHandler: IScheduleHandler) = coRouter {
        "/schedule".nest {
            "/records".nest {
                GET("/{id}", scheduleHandler::find)
                GET("", scheduleHandler::all)
                POST("", scheduleHandler::create)
                PUT("/{id}", scheduleHandler::update)
                DELETE("/{id}", scheduleHandler::delete)
                onError<DatabaseRecordNotFoundException> { _, _ -> notFound().buildAndAwait() }
                onError<AuthorizationException> { e, _ -> status(HttpStatus.FORBIDDEN).json().bodyValueAndAwait(e.localizedMessage) }
                onError<OAuth2AttributeNotFoundException> { _, _ -> status(HttpStatus.FORBIDDEN).buildAndAwait() }
                onError<TimeException> { e, _ -> badRequest().json().bodyValueAndAwait(e.localizedMessage) }
            }
        }
    }
}