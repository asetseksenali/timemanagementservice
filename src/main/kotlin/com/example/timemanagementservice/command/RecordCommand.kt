package com.example.timemanagementservice.command

import org.axonframework.commandhandling.RoutingKey
import org.axonframework.modelling.command.TargetAggregateIdentifier
import java.time.Instant
import java.util.*

sealed interface RecordCommand

data class CreateRecordCommand(
    @RoutingKey val id: UUID,
    val userId: UUID,
    val carWasherId: UUID,
    val startTime: Instant,
    val endTime: Instant
) : RecordCommand

data class ChangeRecordTimeCommand(
    @TargetAggregateIdentifier val id: UUID,
    val userId: UUID,
    val startTime: Instant?,
    val endTime: Instant?
) : RecordCommand

data class DeleteRecordForUserCommand(
    @TargetAggregateIdentifier val id: UUID,
    val userId: UUID
) : RecordCommand

data class DeleteRecordForCarWasherCommand(
    @TargetAggregateIdentifier val id: UUID,
    val carWasherId: UUID
) : RecordCommand
