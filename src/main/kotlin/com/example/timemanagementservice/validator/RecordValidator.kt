package com.example.timemanagementservice.validator

import com.example.timemanagementservice.dto.RecordDTO
import org.springframework.validation.Errors
import org.springframework.validation.ValidationUtils
import org.springframework.validation.Validator

class RecordValidator: Validator {
    override fun supports(clazz: Class<*>): Boolean {
        return RecordDTO::class.java.isAssignableFrom(clazz)
    }

    override fun validate(target: Any, errors: Errors) {
        if (target !is RecordDTO)
            throw IllegalArgumentException("The parameter should not be null and must be of type ${RecordDTO::class.java}")

        when (target) {
            is RecordDTO.Create -> {
                ValidationUtils.rejectIfEmpty(errors, "carWasherId", "carWasherId.required")
                ValidationUtils.rejectIfEmpty(errors, "startTime", "startTime.required")
                ValidationUtils.rejectIfEmpty(errors, "endTime", "endTime.required")
                if (target.startTime.isAfter(target.endTime)) errors.rejectValue("startTime", "startTime.incorrect")
            }
            is RecordDTO.Update -> {
                if (target.startTime != null)
                    if (target.startTime.isAfter(target.endTime))
                        errors.rejectValue("startTime", "startTime.incorrect")
                if (target.endTime != null)
                    if (target.endTime.isBefore(target.startTime))
                        errors.rejectValue("endTime", "endTime.incorrect")
            }
        }
    }
}