package com.example.timemanagementservice.exception

data class ValidationException(val errors: Set<String>): Exception()