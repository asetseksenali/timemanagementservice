package com.example.timemanagementservice.exception

data class AuthorizationException(override val message: String): Exception(message)