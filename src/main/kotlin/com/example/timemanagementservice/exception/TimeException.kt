package com.example.timemanagementservice.exception

data class TimeException(override val message: String) : Exception(message)