create table records(id uuid primary key,
user_id uuid,
car_washer_id uuid,
start_time varchar(80),
end_time varchar(80));

GRANT ALL PRIVILEGES ON all tables in schema public TO vaultuser with grant option ;